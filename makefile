.SUFFIXES: .c .o .h .nw .tex
CC=gcc
CFLAGS= -c 
LDFLAGS=  -lglut -lGLU -lGL  -lXi -lXext -lX11 -lm
DEBUG= -Wall -g
NOWEBS= main.nw family.nw  view.nw list.nw
SOURCES=$(NOWEBS:.nw=.c)
INCLUDES=$(NOWEBS:.nw=.h)
WEAVES=$(NOWEBS:.nw=.tex)
OBJECTS=$(NOWEBS:.nw=.o)

EXECUTABLE=family

all: $(NOWEBS) $(EXECUTABLE) $(SOURCES) $(INCLUDES) $(WEAVES)


%.c: %.nw
	notangle -L -t4 $*.nw > $@ 
	
%.h: %.nw
	notangle  -R$@ $*.nw | cpif $@ 
		
%.tex: %.nw
	noweave -autodefs c -index -t4 $*.nw > $@
	
README.tex: README.nw
	noweave README.nw > $@

%.o: %.c $(INCLUDES)
	$(CC) $(DEBUG) $(CFLAGS) $< -o $@
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(DEBUG) $(OBJECTS) -o $@ $(LDFLAGS)

	
clean:
	rm -rf *.o  $(EXECUTABLE) enclose qhprep	*.c *.h *.tex *.scn *.idx *.pdf *.log *.toc
	
